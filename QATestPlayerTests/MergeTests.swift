//
//  MergeTests.swift
//  QATestPlayerTests
//
//  Created by Igor Grankin on 14.01.2018.
//  Copyright © 2018 Igor Grankin. All rights reserved.
//

import XCTest
import Photos
import AVKit
import AVFoundation

@testable import QATestPlayer
class MergeTests: XCTestCase {
    var mergeManager = MergeManager()
    var mergeWrongManager = MergeManager()
    var asset1 = AVURLAsset(url: URL(string: "file:/var/mobile/Media/DCIM/100APPLE/IMG_0056.MOV")!)
    var asset2 = AVURLAsset(url: URL(string: "file:/var/mobile/Media/DCIM/100APPLE/IMG_0057.MOV")!)

    var asset1wrong = AVURLAsset(url: URL(string: "file:/var/mobile/Media/DCIM/100APPLE/IMG_005677.MOV1")!)
    var asset2wrong = AVURLAsset(url: URL(string: "file:/var/mobile/Media/DCIM/100APPLE/IMG_005777.MOV1")!)
    
    override func setUp() {
        super.setUp()
        mergeManager.assets = [asset1, asset2]
        mergeWrongManager.assets = [asset1wrong, asset2wrong]
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testInit() {
        XCTAssertEqual(mergeManager.assets.first, asset1)
        XCTAssertEqual(mergeManager.assets.last, asset2)
    }
    
    func testCreatingNewRenderSize() {
        XCTAssertEqual(mergeManager.getMaxVideoSize(), true)
        XCTAssertNotEqual(mergeManager.sizeForRender, CGSize(width: 0, height: 0))
    }
    
    func testCreateComposition() {
        //тестируем что получение композиции успешно
        XCTAssertEqual(mergeManager.createCompositionFromAssets(), true)
        //тестируем длительность общей дорожки и сумму дорожек
        XCTAssertEqual(mergeManager.duration, asset1.duration + asset2.duration)
    }

    func testPlayer() {
        mergeManager.playPreparedMergedVideo { (result) in
            XCTAssertEqual(self.mergeManager.mixComposition.tracks.count, 4)
            XCTAssertEqual(self.mergeManager.mainComposition.instructions.count, 1)
        }
    }
    
    func testExport() {
        mergeManager.exportMergedVideo { (result) in
            if (result) {
                XCTAssertEqual(FileManager().fileExists(atPath: self.mergeManager.outputURL.path!), true)
            }
        }
    }

     //MARK - wrong data tests

    func testWrongCreatingNewRenderSize() {
        XCTAssertEqual(mergeWrongManager.getMaxVideoSize(), false)
        XCTAssertEqual(mergeWrongManager.sizeForRender, CGSize(width: 0, height: 0))
    }

    func testWrongCreateComposition() {
        //тестируем что получение композиции завалено
        XCTAssertEqual(mergeWrongManager.createCompositionFromAssets(), false)
        //тестируем длительность общей дорожки и сумму дорожек
        XCTAssertEqual(mergeWrongManager.duration, asset1wrong.duration + asset2wrong.duration)
    }

    func testWrongPlayer() {
        mergeWrongManager.playPreparedMergedVideo { (result) in
            XCTAssertEqual(self.mergeWrongManager.mixComposition.tracks.count, 0)
            XCTAssertEqual(self.mergeWrongManager.mainComposition.instructions.count, 0)
        }
    }

    func testWrongExport() {
        mergeWrongManager.exportMergedVideo { (result) in
            if (result) {
                XCTAssertEqual(FileManager().fileExists(atPath: self.mergeWrongManager.outputURL.path!), false)
            }
        }
    }
}
