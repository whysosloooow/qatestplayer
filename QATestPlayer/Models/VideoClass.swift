//
//  VideoStruct.swift
//  QATestPlayer
//
//  Created by Igor Grankin on 12.01.2018.
//  Copyright © 2018 Igor Grankin. All rights reserved.
//
import UIKit
import Photos

class Video {
    let videoPHAsset: PHAsset
    let thumbnail: UIImage
    let duration: String
    let name: String

    init(asset: PHAsset, thumbnail: UIImage, duration: String, name: String) {
        self.videoPHAsset = asset
        self.thumbnail = thumbnail
        self.duration = duration
        self.name = name
    }
}
