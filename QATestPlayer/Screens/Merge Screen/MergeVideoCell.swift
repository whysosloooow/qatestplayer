//
//  MergeVideoCell.swift
//  QATestPlayer
//
//  Created by Igor Grankin on 12.01.2018.
//  Copyright © 2018 Igor Grankin. All rights reserved.
//

import UIKit

class MergeVideoCell: UITableViewCell {

    @IBOutlet var thumbnailImage: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var durationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
