//
//  MergeTableVC.swift
//  QATestPlayer
//
//  Created by Igor Grankin on 12.01.2018.
//  Copyright © 2018 Igor Grankin. All rights reserved.
//

import UIKit
import Photos
import AVKit
import AVFoundation
import CoreMedia
import UIKit

class MergeTableVC: UITableViewController {

    let merging = MergeManager()

    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet var editButton: UIBarButtonItem!

    @IBAction func editButtonPressed(_ sender: Any) {
        if !tableView.isEditing {
            tableView.setEditing(true, animated: true)
            editButton.title = "Сохранить"
        } else {
            tableView.setEditing(false, animated: true)
            editButton.title = "Изменить"
        }
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "подготовка видео...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        self.present(alert, animated: true, completion: nil)
        merging.playPreparedMergedVideo() {
            (result) in
            if (result) {
                alert.dismiss(animated: true) {
                    let videoItem = AVPlayerItem(asset: self.merging.mixComposition)
                    videoItem.videoComposition = self.merging.mainComposition
                    let player = AVPlayer(playerItem: videoItem)
                    let avpController = AVPlayerViewController()
                    avpController.player = player
                    self.present(avpController, animated: true) {
                        avpController.player!.play()
                    }
            }
            } else {
                alert.dismiss(animated: true) {
                    let actionSheetController: UIAlertController = UIAlertController(title: "Видео еще не готово", message: "повторите позже", preferredStyle: .alert)
                    
                    //Create and add the Cancel action
                    let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
                        //Do some stuff
                    }
                    actionSheetController.addAction(cancelAction)
                    
                    self.present(actionSheetController, animated: true)
                    print("no :(")
            }
    }
    }
    }

    @IBAction func mergeButtonPressed(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "Сохраняю в галерею...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        alert.view.addSubview(loadingIndicator)
        self.present(alert, animated: true, completion: nil)
        
        merging.exportMergedVideo() {
            result in
            if (result) {
                alert.dismiss(animated: true) {
                    let actionSheetController: UIAlertController = UIAlertController(title: nil, message: "видео сохранено!", preferredStyle: .alert)
                    let cancelAction: UIAlertAction = UIAlertAction(title: "Ok", style: .default) { action -> Void in
                    }
                    actionSheetController.addAction(cancelAction)
                    self.present(actionSheetController, animated: true)
                }
            } else {
                alert.dismiss(animated: true) {
                    let actionSheetController: UIAlertController = UIAlertController(title: nil, message: "Упс, что-то пошло не так, наверное Игорь накосячил", preferredStyle: .alert)
                    let cancelAction: UIAlertAction = UIAlertAction(title: "Ok", style: .default) { action -> Void in
                    }
                    actionSheetController.addAction(cancelAction)
                    self.present(actionSheetController, animated: true)
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = 110.0
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return VideoManager.sharedManager.count()
    }

    
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mergeID", for: indexPath) as! MergeVideoCell
            cell.thumbnailImage?.contentMode = .scaleAspectFit
            cell.thumbnailImage?.clipsToBounds = true
            cell.thumbnailImage.image = VideoManager.sharedManager.getAllVideos()[indexPath.row].thumbnail
            cell.nameLabel.text = VideoManager.sharedManager.getAllVideos()[indexPath.row].name
            cell.durationLabel.text = VideoManager.sharedManager.getAllVideos()[indexPath.row].duration


            return cell
        }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
        override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            VideoManager.sharedManager.removeVideoBy(index: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    

    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        VideoManager.sharedManager.move(from: fromIndexPath.row, to: to.row)
    }
    

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
