//
//  PlayerViewController.swift
//  QATestPlayer
//
//  Created by Igor Grankin on 14.01.2018.
//  Copyright © 2018 Igor Grankin. All rights reserved.
//

import UIKit
import Photos
import AVKit
import AVFoundation
import CoreMedia

class PlayerViewController: UIViewController {

    @IBOutlet var playerView: UIView!

    var assetURL: URL!

    
    @IBOutlet var navBar: UINavigationBar!
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let requestOptions = PHVideoRequestOptions()
        requestOptions.version = .original
        PHImageManager.default().requestAVAsset(forVideo: VideoManager.sharedManager.assetToPlay, options: requestOptions) { (asset, audioMix, info) in
            let avURLAsset = asset as! AVURLAsset
                    self.assetURL = avURLAsset.url
            self.navBar.topItem!.title = avURLAsset.url.absoluteString.components(separatedBy: "/").last!.components(separatedBy: ".").first
                    DispatchQueue.main.async {
                        let videoItem = AVPlayerItem(url: self.assetURL)
                        let player = AVPlayer(playerItem: videoItem)
                        let avpController = AVPlayerViewController()
                        avpController.player = player
                        avpController.view.frame = self.playerView.bounds
                        self.addChildViewController(avpController)
                        self.playerView.addSubview(avpController.view)
                        avpController.player?.play()
                    }




        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.destination.isKind(of: UINavigationController.self)) {
            CommentsManager.sharedManager.currentVideoName = self.navBar.topItem!.title!
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
