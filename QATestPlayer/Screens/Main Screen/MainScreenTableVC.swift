//
//  MainScreenTableVC.swift
//  QATestPlayer
//
//  Created by Igor Grankin on 12.01.2018.
//  Copyright © 2018 Igor Grankin. All rights reserved.
//

import UIKit
import Photos
import PhotosUI

class MainScreenTableVC: UITableViewController {
    
    var videos: PHFetchResult<PHAsset>!

    public static var isLoaded = false

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = 110.0
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        MainScreenTableVC.isLoaded = false
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        MainScreenTableVC.isLoaded = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status
            {
            case .authorized:
                print("Good to proceed")
                let fetchOptions = PHFetchOptions()
                self.videos = PHAsset.fetchAssets(with: .video, options: fetchOptions)
                print("Found \(self.videos.count) videos")
            case .denied, .restricted:
                print("Not allowed")
            case .notDetermined:
                print("Not determined yet")
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos != nil ? videos.count : 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "videoMainScreenCellID", for: indexPath) as! VideoMainScreenCell
        let asset = self.videos[indexPath.row]
        let duration = asset.duration
        let requestOptions = PHVideoRequestOptions()
        requestOptions.version = .original

        PHImageManager.default().requestAVAsset(forVideo: asset, options: requestOptions, resultHandler: { (avAsset, audioMix, dict) in
            let avURLAsset = avAsset as! AVURLAsset
            var name = avURLAsset.url.absoluteString.components(separatedBy: "/").last!.components(separatedBy: ".").first
            DispatchQueue.main.async {
                if (name!.characters.count > 10) {
                    let newName = String(name!.prefix(8))

                    cell.nameLabel.text = newName + ".."
                } else {
                    cell.nameLabel.text = name
                }
            }
        })

        cell.durationLabel.text = String(format: "%02d:%02d",Int((duration / 60)),Int(duration) % 60)
        PHImageManager.default().requestImage(for: asset,
                targetSize: CGSize(width: 80,
                        height: 45),
                contentMode: PHImageContentMode.aspectFill,
                options: nil) {
            (image, userInfo) -> Void in
            cell.imageView?.contentMode = .scaleAspectFit
            cell.imageView?.clipsToBounds = true
            cell.imageView?.image = image
            cell.video = Video(asset: asset,
                    thumbnail: image!,
                    duration: cell.durationLabel.text!,
                    name: cell.nameLabel.text!)
            cell.asset = asset

        }
        return cell
    }

//
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if ("playerVC" == segue.identifier) {
//            let player = segue.destination as! PlayerViewController
//        }
//    }


}
