//
//  VideoMainScreenCell.swift
//  QATestPlayer
//
//  Created by Igor Grankin on 12.01.2018.
//  Copyright © 2018 Igor Grankin. All rights reserved.
//

import UIKit
import Photos

class VideoMainScreenCell: UITableViewCell {
    
    var asset: PHAsset!
    var video: Video!
    
    @IBOutlet var thumbnailImage: UIImageView!
    
    @IBOutlet var nameLabel: UILabel! {
        didSet {
            if (self.nameLabel.text!.count > 10) {
                print("more 10 symbols")
            }
        }
    }
    @IBOutlet var durationLabel: UILabel!

    @IBAction func playButtonPressed(_ sender: Any) {
        VideoManager.sharedManager.assetToPlay = self.asset
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if (self.isSelected) {
            self.contentView.backgroundColor = .green
            if (!VideoManager.sharedManager.contains(name: self.video.name)) {
                VideoManager.sharedManager.addVideo(currentVideo: self.video)
                print(VideoManager.sharedManager.getAllVideos())
            }
        } else if (!self.isSelected && MainScreenTableVC.isLoaded) {
            self.contentView.backgroundColor = .white
            if (VideoManager.sharedManager.contains(name: self.video.name)) {
                VideoManager.sharedManager.removeVideo(currentVideo: self.video)
                print(VideoManager.sharedManager.getAllVideos())
            } else if (!self.isSelected && !MainScreenTableVC.isLoaded) {
                    self.contentView.backgroundColor = .white

            }
        }
        // Configure the view for the selected state
    }

}
