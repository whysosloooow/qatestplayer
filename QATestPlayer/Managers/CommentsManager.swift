//
//  CommentsManager.swift
//  QATestPlayer
//
//  Created by Igor Grankin on 23.01.2018.
//  Copyright © 2018 Igor Grankin. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CommentsManager {

    var currentVideoName = String()
    static let sharedManager = CommentsManager()
    var videoComments = [NSManagedObject]()

    private init() {
    }

    func add(text commentString: String, forName nameString: String) {
        let appDelegate =
                UIApplication.shared.delegate as! AppDelegate

        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        let entity = NSEntityDescription.entity(forEntityName: "Comment",
                in:
                managedContext)

        let comment = NSManagedObject(entity: entity!,
                insertInto:managedContext)

        comment.setValue(nameString, forKey: "VideoName")
        comment.setValue(commentString, forKey: "text")

        do {
           try managedContext.save()
        } catch let error as NSError {
            print("error: \(error)")
        }

}

    func getAllComments() {
        let appDelegate =
                UIApplication.shared.delegate as! AppDelegate

        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        //2
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Comment")
        fetchRequest.predicate = NSPredicate(format: "(videoName == %@)", self.currentVideoName)
        var fetchedResults: [NSManagedObject]?
        //3
        var error: NSError?
        do {
            try fetchedResults =
                    managedContext.fetch(fetchRequest) as? [NSManagedObject]
        } catch let error as NSError {
            print("error: \(error)")
        }

        if let results = fetchedResults {
            videoComments = fetchedResults!
        } else {
            print("Could not fetch \(error), \(error!.userInfo)")
        }
    }
}
