//
//  MergeManager.swift
//  QATestPlayer
//
//  Created by Igor Grankin on 14.01.2018.
//  Copyright © 2018 Igor Grankin. All rights reserved.
//

import Foundation
import Photos
import AVKit
import AVFoundation
import CoreMedia
import UIKit

class MergeManager {
    
    var videoSet: [Video]!
    
    var assets = [AVURLAsset]()
    
    var arrayInstruction = [AVVideoCompositionLayerInstruction]()
    
    var mainInstruction  = AVMutableVideoCompositionInstruction()
    
    var mixComposition = AVMutableComposition()
    
    var mainComposition: AVMutableVideoComposition = AVMutableVideoComposition()
    
    var duration:CMTime = kCMTimeZero
    
    var outputURL = NSURL()
    
    var sizeForRender: CGSize = CGSize(width: 0, height: 0)
    
    init() {
    }
    
    func getMaxVideoSize() -> Bool {
        for elem in assets {
            if (elem.tracks.count > 0) {
                var currentSize = elem.tracks.first!.naturalSize.applying(elem.tracks.first!.preferredTransform)
                currentSize = CGSize(width: fabs(currentSize.width), height: fabs(currentSize.height))
                if (currentSize.width > sizeForRender.width || currentSize.height > sizeForRender.height) {
                    sizeForRender = currentSize
                }
            }
        }
        if (sizeForRender.width > 0 && sizeForRender.height > 0) {
            print("new size for render")
            return true
        } else {
            print("no new size for render")
            return false
        }
    }
    
    func getAssetsFromVideoManager(completion:@escaping (_ result: Bool) -> Void) {
        if (VideoManager.sharedManager.count() == 0 && self.assets.count > 0) {
            completion(true)
        }
        videoSet = [Video]()
        assets = [AVURLAsset]()
        arrayInstruction = [AVVideoCompositionLayerInstruction]()
        mainInstruction  = AVMutableVideoCompositionInstruction()
        mixComposition = AVMutableComposition()
        duration = kCMTimeZero
        if (VideoManager.sharedManager.count() > 0) {
            //Creating options
            let requestOptions = PHVideoRequestOptions()
            requestOptions.version = .original
            //Iterating each element of VideoManager
            DispatchQueue.main.async {
                let downloadGroup = DispatchGroup()
                
                for index in 0..<VideoManager.sharedManager.count() {
                    downloadGroup.enter()
                    PHImageManager.default().requestAVAsset(forVideo: VideoManager.sharedManager.getVideoAtIndex(index: index).videoPHAsset,
                                                            options: requestOptions,
                                                            resultHandler: { (avAsset, audioMix, dict) in
                                                                let avURLAsset = avAsset as! AVURLAsset
                                                                self.assets.append(avURLAsset)
                                                                downloadGroup.leave()
                    })
                }
                downloadGroup.wait()
            }
            DispatchQueue.main.async {
                if (self.assets != nil) {
                    if (self.assets.count == VideoManager.sharedManager.count()) {
                        completion(true)
                    } else {
                        completion(false)
                    }
                }
            }
        }
    }
    
    func createCompositionFromAssets() -> Bool {
        if (getMaxVideoSize()) {
            for elem in self.assets {
                let currentAsset: AVAsset = AVAsset(url: elem.url)
                let currentTrack: AVMutableCompositionTrack = self.mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)!
                let currentAudioTrack: AVMutableCompositionTrack = self.mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)!
                do {
                    try currentTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, currentAsset.duration), of: currentAsset.tracks(withMediaType: .video)[0], at: duration)
                    try currentAudioTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, currentAsset.duration), of: currentAsset.tracks(withMediaType: .audio)[0], at: duration)
                } catch let error as NSError {
                    print("error: \(error)")
                }
                let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: currentTrack)
                //moving video screen
                let moveByX = fabs(currentTrack.naturalSize.applying((currentAsset.tracks.first?.preferredTransform)!).width / 2)
                let moveByY = fabs(currentTrack.naturalSize.applying((currentAsset.tracks.first?.preferredTransform)!).height / 2)
                
                let moveByXAffineTransform = CGAffineTransform(translationX: self.sizeForRender.width / 2 - moveByX, y: 0)
                let moveByYAffineTransform = CGAffineTransform(translationX: 0, y: self.sizeForRender.height / 2 - moveByY)
                //scaling video screen
                //                let scaleByX = self.sizeForRender.width / fabs(currentTrack.naturalSize.applying((currentAsset.tracks.first?.preferredTransform)!).width)
                //                let scaleByY = self.sizeForRender.height / fabs(currentTrack.naturalSize.applying((currentAsset.tracks.first?.preferredTransform)!).height)
                //
                //                var scaleAffineTransform = CGAffineTransform()
                //
                //                if (scaleByX > 1 && scaleByY > 1) {
                //                    scaleAffineTransform = CGAffineTransform(scaleX: scaleByX, y: scaleByY)
                //                } else {
                //                    scaleAffineTransform = CGAffineTransform(scaleX: 0, y: 0)
                //                }
                
                let rotateAffineTransform = (currentAsset.tracks.first?.preferredTransform)!
                //TODO: add scaling
                instruction.setTransform(rotateAffineTransform
                    .concatenating(moveByXAffineTransform)
                    .concatenating(moveByYAffineTransform),
                                         at: kCMTimeZero)
                duration = CMTimeAdd(duration, currentAsset.duration)
                instruction.setOpacity(0.0, at: duration)
                arrayInstruction.append(instruction)
            }
            if (arrayInstruction.count == assets.count) {
                self.mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, self.duration)
                self.mainInstruction.layerInstructions = self.arrayInstruction
                let arrayOfMainInstruction = [self.mainInstruction]
                self.mainComposition.instructions = arrayOfMainInstruction
                self.mainComposition.renderSize = CGSize(width: self.sizeForRender.width,
                                                         height: self.sizeForRender.height)
                self.mainComposition.frameDuration = CMTime(value: 1, timescale: 30)
                print("\n duration is ", self.duration.value, "\n")
                return true
            }
        }
        return false
    }
    
    func playPreparedMergedVideo(completion: @escaping (_ result: Bool) -> Void) {
        getAssetsFromVideoManager() {
            (result) in
            if (result) {
                print("hooray!")
                if (self.createCompositionFromAssets()) {
                    completion(true)
                }
            } else {
                completion(false)
            }
        }
    }
    
    func exportMergedVideo(completion:@escaping (_ result: Bool) -> Void) {
        getAssetsFromVideoManager() {
            (result) in
            if (result) {
                print("access granted")
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .long
                dateFormatter.timeStyle = .short
                let date = dateFormatter.string(from: NSDate() as Date)
                let savePath = NSTemporaryDirectory().appendingFormat("mergeVideo.mov")
                let url = NSURL(fileURLWithPath: savePath)
                self.outputURL = url
                if (FileManager().fileExists(atPath: url.path!)) {
                    do {
                        try FileManager().removeItem(at: url as URL)
                        print("some video was at path")
                    } catch let error as NSError {
                        print("error: \(error)")
                    }
                }
                if (self.createCompositionFromAssets()) {
                    // 5 - Create Exporter
                    guard let exporter = AVAssetExportSession(asset: self.mixComposition, presetName: AVAssetExportPresetHighestQuality) else {
                        return
                    }
                    exporter.outputURL = url as URL
                    exporter.outputFileType = .mov
                    exporter.videoComposition = self.mainComposition
                    exporter.shouldOptimizeForNetworkUse = true
                    exporter.timeRange = CMTimeRangeMake(kCMTimeZero, self.duration)
                    
                    // 6 - Perform the Export
                    exporter.exportAsynchronously() {
                        
                        PHPhotoLibrary.shared().performChanges({
                            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url as URL)
                        }) { saved, error in
                            switch exporter.status {
                            case AVAssetExportSessionStatus.completed:
                                    print("-----Merge mutable video with trimmed audio exportation complete.\(String(describing: url))")
                                completion(true)
                            case AVAssetExportSessionStatus.failed:
                                    print("failed \(String(describing: exporter.error))")
                                completion(false)
                            case AVAssetExportSessionStatus.cancelled:
                                print("cancelled \(String(describing: exporter.error))")
                                completion(false)
                            default:
                                print("complete")
                                completion(false)
                            }
                        }
                        
                    }
                }
            } else {
                    print("no :(")
                    completion(false)
            }
        }
    }
}
