//
//  VideoManager.swift
//  QATestPlayer
//
//  Created by Igor Grankin on 12.01.2018.
//  Copyright © 2018 Igor Grankin. All rights reserved.
//

import UIKit
import Photos

class VideoManager {
    
    private var videoSet = [Video]()

    var assetToPlay: PHAsset!
    
    static let sharedManager = VideoManager()
    
    private init() {
    }

    func addVideo(currentVideo: Video) {
         videoSet.append(currentVideo)
    }
    
    func removeVideo(currentVideo: Video) {
        let index = videoSet.index{$0 === currentVideo}
        if (index != nil) {
            videoSet.remove(at: index!)
        }
    }
    func removeVideoBy(index: Int) {
        if index < count() {
            videoSet.remove(at: index)
        }
    }
    
    func getAllVideos() -> [Video] {
        return videoSet
    }
    
    func getVideoAtIndex(index: Int) -> Video {
            return videoSet[index]
    }

    func count() -> Int {
        return videoSet.count
    }

    func contains(name: String) -> Bool {
        for elem in videoSet {
            if elem.name == name {
                return true
            }
        }
        return false
    }

    func move(from startIndex: Int, to finishIndex: Int) {
        if (startIndex <= count() && finishIndex <= count()) {
            let currentVideo = getVideoAtIndex(index: startIndex)
            removeVideoBy(index: startIndex)
            videoSet.insert(currentVideo, at: finishIndex)
        }
    }
}
